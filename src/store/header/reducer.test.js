import reducer from "./reducer";
import { IS_MODAL_OPEN, CHOSEN_TO_OPERATE, ADD_TO_FAVORITE, MOVE_FROM_FAVORITE } from "./actions";

const initialState = {
  array: [],
  isModalOpen: false,
  chosenToOperate: {},
};

describe("reducer works", () => {
  test("should return the initial state", () => {
    expect(reducer(undefined, { type: undefined })).toEqual(initialState);
  });

  test("should change isOpen", () => {
    expect(reducer(initialState, { type: IS_MODAL_OPEN, payload: true })).toEqual(
      {
        array: [],
        isModalOpen: true,
        chosenToOperate: {},
      }
    );
  });
  
test("should chosen item change array", () => {
 expect(reducer(initialState, {type: CHOSEN_TO_OPERATE,
      payload: { name: "AAAA", art: 11111111 }})).toEqual({
    array: [],
    isModalOpen: false,
    chosenToOperate: { name: "AAAA", art: 11111111 },
  });
});

  test("should add to favorite change array", () => {
   
 expect(
   reducer(
     {
       array: [
         { name: "AAA", art: 111, isFavorite: false },
         { name: "BBB", art: 222, isFavorite: false },
       ],
       isModalOpen: false,
       chosenToOperate: {},
     },
     { type: ADD_TO_FAVORITE, payload:  111   }
   )
 ).toEqual({
   array: [
     { name: "AAA", art: 111, isFavorite: true },
     { name: "BBB", art: 222, isFavorite: false},
   ],
   isModalOpen: false,
   chosenToOperate: {},
 });
});   
 test("should move from favorite change array", () => {
   expect(
     reducer(
       {
         array: [
           { name: "AAA", art: 111, isFavorite: true },
           { name: "BBB", art: 222, isFavorite: false },
         ],
         isModalOpen: false,
         chosenToOperate: {},
       },
       { type: MOVE_FROM_FAVORITE, payload: 111 }
     )
   ).toEqual({
     array: [
       { name: "AAA", art: 111, isFavorite: false },
       { name: "BBB", art: 222, isFavorite: false },
     ],
     isModalOpen: false,
     chosenToOperate: {},
   });
 });      
});
