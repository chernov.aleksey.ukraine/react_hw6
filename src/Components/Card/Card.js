import React from "react";
import "./Card.scss";
import {useDispatch } from "react-redux";
import { addToFavoriteAC, chosenToOperateAC, isModalOpenAC, moveFromFavoriteAC } from "../../store/header/actionCreators";


const Card = ({
   
  name,
  art,
  color,
  price,
  url,
  isCart,
  isFavorite,
   
 
}) => {
  


  const dispatch = useDispatch();

 


  return (
    <div className="itemcard">
      <div className="itemcardheader">
        <p className="itemart">code: {art}</p>
        <div className="iconcontainer">
          {isFavorite ? (
            <p
              onClick={() => {
             dispatch(moveFromFavoriteAC(art));
                
              }}
            >
              &#9733;
            </p>
          ) : (
            <p
                onClick={() => {
              dispatch(addToFavoriteAC(art));
             
              }}
            >
              &#9734;
            </p>
          )}
          <div className="carticonholder">
            {isCart ? (
              <p
                onClick={() => {
                 
                }}
              >
                IN CART
              </p>
            ) : (
              <img
                  onClick={() => {
                    dispatch(chosenToOperateAC({name,art}))
                  // chooseItem({ art, name });

                  dispatch(isModalOpenAC(true));
                }}
                src="./cart.png"
                alt=""
              />
            )}
          </div>
        </div>
      </div>
      <div className="itemphotocontainer">
        <img src={url} alt="img" />
      </div>
      <p className="itemname">{name}</p>
      <p className="itemcolor">color: {color}</p>
      <p className="itemprice">
        {price} <span>гр.</span>
      </p>
    </div>
  );
};

Card.propTypes = {
};
export default Card;

