import Button from './Button';
import { render, screen, fireEvent  } from "@testing-library/react";
const onClick = jest.fn();

describe('Button snapshot testing', () => {
    test('should Button match snapshot', () => {
        const { asFragment } = render(<Button  onClick={onClick} text="text1" />);
        expect(asFragment()).toMatchSnapshot();
    })
})

describe("Handle click at a button", () => {
    test("should handleClick work", () => {
        render(<Button   onClick={onClick} text="text1" />);
        const btn = screen.getByTestId("Button");
        fireEvent.click(btn)
        expect(onClick).toHaveBeenCalled();
    })
})
