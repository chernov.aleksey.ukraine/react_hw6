import React from "react";
import "./Button.scss";
import PropTypes from "prop-types";

const Button = ({onClick, text}) => {
  
    return (
      <div>
        <button data-testid='Button' onClick={onClick}>{text}</button>
      </div>
    );
   
}

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
    .isRequired,
};

export default Button;

