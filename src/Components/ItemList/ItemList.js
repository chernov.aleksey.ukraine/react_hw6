import React, { useContext} from "react";
import Card from "../Card/Card";
import Line from "../Line/Line";
import "./ItemList.scss";
import { useSelector } from "react-redux";
import ReflectionContext from "../../contexts/ReflectionContext/ReflectionContext";

const ItemList = () => {
  
  const array = useSelector((store) => store.header.array);
   const { cardReflection } = useContext(ReflectionContext);
  



    return (
      <div className="itemcontainer">
       
        {array.map(({ name, art, color, price, url, isFavorite, isCart }) =>
          cardReflection === "tab" ? (
            <Line
              key={art}
              name={name}
              art={art}
              color={color}
              price={price}
              url={url}
              isFavorite={isFavorite}
              isCart={isCart}
            />
          ) : (
            <Card
              key={art}
              name={name}
              art={art}
              color={color}
              price={price}
              url={url}
              isFavorite={isFavorite}
              isCart={isCart}
            />
          )
        )}
      </div>
    );
  
}
ItemList.propTypes = {};
export default ItemList;
