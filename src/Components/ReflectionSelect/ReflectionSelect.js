import React, {useContext } from "react";
import ReflectionContext from "../../contexts/ReflectionContext/ReflectionContext";
import  './ReflectionSelect.scss'

const ReflectionSelect = () => {
  // const [value, setValue] = useState('EN');
  const { cardReflection, setCardReflection } = useContext(ReflectionContext);

  return (
    <div className="selectionOfReflection">
      <select
        name="Type of Reflection"
        id="reflection"
        value={cardReflection}
        onChange={({ target: { value } }) => setCardReflection(value)}
      >
        <option value="card">Card Reflection Mode</option>
        <option value="tab">Tab Reflection Mode</option>
      </select>
    </div>
  );
};

export default ReflectionSelect;
