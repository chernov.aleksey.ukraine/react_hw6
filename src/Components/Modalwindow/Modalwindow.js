import React from "react";
import "./Modalwindow.scss";
import Button from "../Button/Button";
import PropTypes from "prop-types";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { isModalOpenAC, addToCartAC ,moveFromCartAC} from "../../store/header/actionCreators";

const Modalwindow = ({
      operation,
      headertext,
      maintext1,
      maintext2,
}) => {
  const dispatch = useDispatch();
  const isModalOpen = useSelector((store) => store.header.isModalOpen);
  const chosenItem = useSelector((store) => store.header.chosenToOperate, shallowEqual);
    if (!isModalOpen) return null;

      return (
        <>
          <div
            data-testid="modal-root"
            className="globalback"
            onClick={() => {
              dispatch(isModalOpenAC(false));
            }}
          ></div>

          <div className="modal">
            <header className="modalheader">
              <p>{headertext}</p>

              <div
                className="crossbutton"
                onClick={() => {
                  dispatch(isModalOpenAC(false));
                }}
              >
                &#10006;
              </div>
            </header>
            <main className="modalmain">
              <div>
                {" "}
                {maintext1} " {chosenItem.name} " {maintext2}{" "}
              </div>
            </main>
            <div className="modalbuttoncontainer">
              <Button
                onClick={() => {
                  if (operation === "add") {
                    dispatch(addToCartAC(chosenItem.art));
                  } else {
                    dispatch(moveFromCartAC(chosenItem.art));
                  }

                  dispatch(isModalOpenAC(false));
                }}
                text={"OK"}
              />
              <Button
                onClick={() => {
                  dispatch(isModalOpenAC(false));
                }}
                text={"CLOSE"}
              />
            </div>
          </div>
        </>
      );
    }
Modalwindow.propTypes = {
  headertext: PropTypes.string.isRequired,
  maintext1: PropTypes.string.isRequired,
  maintext2: PropTypes.string.isRequired,
  operation: PropTypes.string.isRequired,
  };

export default Modalwindow;
