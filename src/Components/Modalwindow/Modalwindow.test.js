import Modalwindow from "./Modalwindow";
import { render, screen, fireEvent } from "@testing-library/react";
import { Provider, useDispatch } from "react-redux";
import store from "../../store/index";
import { isModalOpenAC } from "../../store/header/actionCreators";

const Component = () => {
  const dispatch = useDispatch();
  return (
    <>
      <button onClick={() => { dispatch(isModalOpenAC(true)) }}>OPEN</button>
      <button onClick={() => { dispatch(isModalOpenAC(false))}}>CLS-button</button>
      <Modalwindow operation="add" maintext1="maintext1" maintext2="maintext2" headertext="headertext" />
    </>
  );
};
const MockedProvider = () => (
  <Provider store={store}>
    <Component />
  </Provider>
);
const RenderComponent = () => {
  return (
    <Provider store={store}>
      <Modalwindow operation="add" maintext1="maintext1" maintext2="maintext2" headertext="headertext" />
    </Provider>
  );
};


describe("Modal renders", () => {
  test("should Modal match snapshot", () => {
      const { asFragment } = render(<RenderComponent />);
    expect(asFragment()).toMatchSnapshot();
  });
});


describe("Modal open on state chnages", () => {
  test("should Modal opens on state change", () => {
    render(<MockedProvider />);
    fireEvent.click(screen.getByText("OPEN"));
    expect(screen.getByTestId("modal-root")).toBeInTheDocument();
  });
});

describe("Modal closes on state chnages", () => {
  test("should Modal closes on state change", () => {
    render(<MockedProvider />);
    fireEvent.click(screen.getByText("CLS-button"));
    expect(screen.queryByTestId("modal-root")).not.toBeInTheDocument();
  });
});

describe("Modal closes on click to the screen out of modal window", () => {
  test("should Modal closes on click out of the modal window", () => {
    render(<MockedProvider />);
    fireEvent.click(screen.getByText("OPEN"));
    fireEvent.click(screen.getByTestId("modal-root"));
    expect(screen.queryByTestId("modal-root")).not.toBeInTheDocument();
  });
});