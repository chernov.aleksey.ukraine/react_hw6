import React from "react";
import Card from "../Card/Card";
import "./FavoriteList.scss";
import { useSelector,} from "react-redux";

const FavoriteList = () => {
  const array = useSelector((store) => store.header.array);
  
  return (
    <div className="itemcontainer">
      {array.map(({ name, art, color, price, url, isFavorite, isCart }) => 
        isFavorite ? (
          <Card
            key={art}
            name={name}
            art={art}
            color={color}
            price={price}
            url={url}
            isFavorite={isFavorite}
            isCart={isCart}
          />
        ) : null
      )}
    </div>
  );
};
FavoriteList.propTypes = {};
export default FavoriteList;
