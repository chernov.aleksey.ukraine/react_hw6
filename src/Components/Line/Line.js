import React from "react";
 import "./Line.scss";
import {useDispatch } from "react-redux";
import { addToFavoriteAC, chosenToOperateAC, isModalOpenAC, moveFromFavoriteAC } from "../../store/header/actionCreators";


const Line = ({
   
  name,
  art,
  color,
  price,
  url,
  isCart,
  isFavorite,
   
 
}) => {
  


  const dispatch = useDispatch();

 


  return (
    <div className="mainLine">
      {isFavorite ? (
        <p
          className="lineStar"
          onClick={() => {
            dispatch(moveFromFavoriteAC(art));
          }}
        >
          &#9733;
        </p>
      ) : (
        <p
          className="lineStar"
          onClick={() => {
            dispatch(addToFavoriteAC(art));
          }}
        >
          &#9734;
        </p>
      )}
      <div className="lineCartImgContainer">
        {isCart ? (
          <p onClick={() => {}}>IN CART</p>
        ) : (
          <img
            className="lineCartImg"
            onClick={() => {
              dispatch(chosenToOperateAC({ name, art }));
              // chooseItem({ art, name });

              dispatch(isModalOpenAC(true));
            }}
            src="./cart.png"
            alt=""
          />
        )}
      </div>
      <div className="lineImage">
        <img src={url} alt="img" />
      </div>
      <p className="lineInfo lineName">{name}</p>
      <p className="lineInfo">color: {color}</p>
      <p className="lineInfo">  {price} <span>гр.</span></p>
      <p className="lineInfo">code: {art}</p>
    </div>
  );
};

Line.propTypes = {
};
export default Line;

