import "./App.scss";
import Header from "./Components/Header/Header";
import { BrowserRouter } from "react-router-dom";
import AppRoutes from "./Routes";
import { Provider } from "react-redux";
import store from "./store";
import ReflectionContextProvider from "./contexts/ReflectionContext/ReflectionContextProvider";

const App = () => {
  

  return (
    <Provider store={store}>
      <BrowserRouter>
        <ReflectionContextProvider>
          <div className="App">
            <Header />
            <AppRoutes />
          </div>
        </ReflectionContextProvider>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
