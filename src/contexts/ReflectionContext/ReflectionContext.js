import { createContext } from "react";

const ReflectionContext = createContext();

export default ReflectionContext;
