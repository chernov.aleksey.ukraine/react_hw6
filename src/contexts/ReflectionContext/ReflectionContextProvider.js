import { useState } from "react";
import ReflectionContext from "./ReflectionContext";

const ReflectionContextProvider = ({ children }) => {
  const [cardReflection, setCardReflection] = useState('card');

  return (
    <ReflectionContext.Provider
      value={{ cardReflection, setCardReflection }}
    >
      {children}
    </ReflectionContext.Provider>
  );
};

export default ReflectionContextProvider;
