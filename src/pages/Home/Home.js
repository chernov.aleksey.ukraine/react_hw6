import React from "react";
import ItemList from '../../Components/ItemList/ItemList'
import Modalwindow from "../../Components/Modalwindow/Modalwindow";
import ReflectionSelect from "../../Components/ReflectionSelect/ReflectionSelect";
const Home = () => {
    
    return (
      <div>
        <ReflectionSelect />
        <ItemList />
        <Modalwindow
          operation={"add"}
          headertext={"Adding item to the cart."}
          maintext1={"Do you want to add"}
          maintext2={"to the cart?"}
        />
      </div>
    );
 }



export default Home;
